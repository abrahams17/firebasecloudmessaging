import { Component } from "@angular/core";
import { Platform } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { FcmService } from "./services/fcm.service";
import { Plugins, Capacitor } from "@capacitor/core";
@Component({
	selector: "app-root",
	templateUrl: "app.component.html",
	styleUrls: ["app.component.scss"],
})
export class AppComponent {
	constructor(
		private platform: Platform,
		private fcmService: FcmService
	) {
		this.initializeApp();
	}

	initializeApp() {
		this.hideSplashScreenAfterShown();
	}
	public hideSplashScreenAfterShown() {
		this.platform.ready().then(() => {
			if (Capacitor.isPluginAvailable("SplashScreen")) {
				Plugins.SplashScreen.hide();
				// Trigger the push setup
				this.fcmService.initPush();
			}
		});
	}
}
